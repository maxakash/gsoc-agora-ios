Below are the mockups of the Agora - iOS App.



Splash             
:-------------------------:
![](https://i.imgur.com/yNKsGtY.png?1) 


Login  light         |  Login Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/FCq4N9n.png)  |  ![](https://i.imgur.com/0ctGbem.png)


Sign Up light            |  Sign Up Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/LrJGHS4.png)  |  ![](https://i.imgur.com/BBolvRm.png)


Forgot Password light             |  Forgot Password Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/FCq4N9n.png)  |  ![](https://i.imgur.com/MMa5Ayx.png)


Dashboard light            |  Dashboard Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/UbZDipp.png)  |  ![](https://i.imgur.com/MizHRL2.png)


Create Election(election details) light            |  Create Election(election details) Up Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/BILt8y3.png)  |  ![](https://i.imgur.com/nFWwueF.png)


Unfilled details error light            
:-------------------------:
![](https://i.imgur.com/BGpKiQa.png?1)  


Create Election(date picker) light            |  Create Election(date picker) Up Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/E9dyqYO.png)  |  ![](https://i.imgur.com/n1FADQ3.png)


Create Election(Add Candidates) light            |  Create Election(Add Candidates) Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/tMOJ2tV.png)  |  ![](https://i.imgur.com/skwjjFc.png)



Create Election(Voting Algorithm) light            |  Create Election(Voting Algorithm) Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/xL7iwX4.png)  |  ![](https://i.imgur.com/bfdcZw4.png)



Elections(Active) light            |  Elections(Active) Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/fjHMtw6.png)  |  ![](https://i.imgur.com/u2vm07Z.png)



Elections(Pending) light            |  Elections(Pending) Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/gl63X1I.png)  |  ![](https://i.imgur.com/7Gejvhn.png)



Elections(Finished) light            |  Elections(Finished) Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/0XeaAa3.png)  |  ![](https://i.imgur.com/b59Hkxm.png)



Election details light            |  Election details Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/VyRn5Sj.png)  |  ![](https://i.imgur.com/NjX3t7Y.png)



Edit election light            |  Edit election Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/x4UkTbi.png)  |  ![](https://i.imgur.com/zrJYU6W.png)



Add Voters            |  Add Voters   Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/HKfhxcp.png)  |  ![](https://i.imgur.com/51KbRS3.png)



Result light            |  Result Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/OeJHqYC.png)  |  ![](https://i.imgur.com/NPpq33b.png)



Profile light            |  Profile Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/GtSPFab.png)  |  ![](https://i.imgur.com/3NbXFST.png)



About Agora light            |  About Agora  Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/28eZ5zS.png)  |  ![](https://i.imgur.com/o9VzzKL.png)



Contact Us light            |  Contact Us Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/SkyM0HK.png)  |  ![](https://i.imgur.com/eF5d4PP.png)




Voting Ballot 1 light            |  Voting Ballot 1 Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/GNA0Au7.png)  |  ![](https://i.imgur.com/hfmN9xA.png)



Voting Ballot 2 light            |  Voting Ballot 2 Dark
:-------------------------:|:-------------------------:
![](https://i.imgur.com/FK03ivj.png)  |  ![](https://i.imgur.com/JWPB0Tq.png)